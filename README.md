﻿# Draughts

Draughts is a version of the board game checkers and is based on "Privacy Friendly Checkers" developed by Technische Universität Darmstadt, Germany.
The original work was modified by Blocks Global Pty Ltd for its Lumin screen users.
The original work is hosted on https://github.com/SecUSo/privacy-friendly-dame

## Download and more Information

Further development requires Android Studio, we recommend to use at least version 3.1.0
 
### API Reference

Mininum SDK: 22
Target SDK: 26 

## License

Draughts is licensed under the GPLv3.
Copyright (C) 2017-2018  Maksim Melnik, Marc Schneider

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

The icons used in the nagivation drawer are licensed under the [CC BY 2.5](http://creativecommons.org/licenses/by/2.5/). In addition to them the app uses icons from [Google Design Material Icons](https://design.google.com/icons/index.html) licensed under Apache License Version 2.0. The crown on the pieces to mark the King is licened under the SIL OPEN FONT LICENSE Version 1.1. All other images (the logo of Privacy Friendly Apps, the SECUSO logo, the app logo and the game pieces) copyright [Technische Universtität Darmstadt](www.tu-darmstadt.de) (2018).

## Contributors

App-Icon: <br />
Tatjana Albrandt<br />

Github-Users: <br />
Yonjuni (Karola Marky)<br />
Kamuno (Christopher Beckmann)<br />
keksoe (Maksim Melnik)<br />
marcsn (Marc Schneider)

Blocks Global Pty Ltd


