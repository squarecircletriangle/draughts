/*
 This file is part of Draughts.

 Draughts is free software:
 you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.

 Draughts is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This file was modified by Blocks Global Pty Ltd (http://www.blocksglobal.com/) on 14/01/2019
*/

package com.blocksglobal.lumin.draughts;

import com.blocksglobal.lumin.draughts.game.CheckersGame;
import com.blocksglobal.lumin.draughts.game.GameType;
import com.blocksglobal.lumin.draughts.game.Move;
import com.blocksglobal.lumin.draughts.game.Piece;
import com.blocksglobal.lumin.draughts.game.Position;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class CheckersGameTest {

    CheckersGame game;

    @Before
    public void init() {
        this.game = new CheckersGame(GameType.Human);
    }

    @Test
    public void checkLegalMoves() {
        assertEquals(game.whoseTurn(), CheckersGame.WHITE);
        Move[] moves = {
                new Move(new Position(0, 5)).add(new Position(1, 4)),
                new Move(new Position(2, 5)).add(new Position(1, 4)),
                new Move(new Position(2, 5)).add(new Position(3, 4)),
                new Move(new Position(4, 5)).add(new Position(3, 4)),
                new Move(new Position(4, 5)).add(new Position(5, 4)),
                new Move(new Position(6, 5)).add(new Position(5, 4)),
                new Move(new Position(6, 5)).add(new Position(7, 4)),
        };
        Move[] ingame = game.getMoves();
        for (int i = 0; i < moves.length; i++) {
            assertTrue(ingame[i].equals(moves[i]));
        }
    }

    @Test
    public void checkMove() {
        Piece piece1 = game.getBoard().getPiece(1, 2);
        game.makeMove(new Move(new Position(1, 2)).add(new Position(2, 3)));
        Piece piece2 = game.getBoard().getPiece(2, 3);
        assertEquals(piece1, piece2);
    }

    @Test
    public void checkCapture() {
        assertEquals(game.whoseTurn(), CheckersGame.WHITE);
        game.makeMove(new Move(new Position(1, 2)).add(new Position(2, 3)));
        assertEquals(game.whoseTurn(), CheckersGame.BLACK);
        game.makeMove(new Move(new Position(4, 5)).add(new Position(3, 4)));
        assertEquals(game.whoseTurn(), CheckersGame.WHITE);
        assertEquals(1, game.getMoves().length);
        Move move = game.getMoves()[0];
        assertEquals(1, move.capturePositions.size());
        Piece willBeCaptured = game.getBoard().getPiece(3, 4);
        assertNotNull(willBeCaptured);
        game.makeMove(move);
        assertNull(game.getBoard().getPiece(3, 4));
    }
}