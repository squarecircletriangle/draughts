/*
 Copyright 2019 Blocks Global Pty Ltd (http://www.blocksglobal.com/)

 This file is part of Draughts.

 Draughts is free software:
 you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.

 Draughts is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package com.blocksglobal.lumin.draughts.app;

import static com.blocksglobal.lumin.draughts.utils.LuminUtils.getLuminUser;

import android.app.Application;

import com.blocksglobal.lumin.draughts.BuildConfig;
import com.blocksglobal.lumin.draughts.model.LuminUser;
import com.bugsnag.android.Bugsnag;
import com.bugsnag.android.Configuration;

public class Draughts extends Application {

    public static final String ANALYTICS_APP_LAUNCH = "Draughts Launched";

    @Override
    public void onCreate() {
        super.onCreate();
        Configuration configuration = new Configuration(BuildConfig.BUGSNAG_KEY);
        LuminUser luminUser = getLuminUser(this);
        if (luminUser != null) {
            configuration.setUser(luminUser.getUuid(), luminUser.getEmail(), luminUser.getName());
        }
        Bugsnag.start(this, configuration);
    }
}
