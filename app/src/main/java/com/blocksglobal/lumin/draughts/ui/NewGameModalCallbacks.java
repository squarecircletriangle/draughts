/*
 Copyright 2019 Blocks Global Pty Ltd (http://www.blocksglobal.com/)

 This file is part of Draughts.

 Draughts is free software:
 you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.

 Draughts is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package com.blocksglobal.lumin.draughts.ui;


import com.blocksglobal.lumin.draughts.game.GameType;

public interface NewGameModalCallbacks {
    void onStartGame(GameType gameType);
}
