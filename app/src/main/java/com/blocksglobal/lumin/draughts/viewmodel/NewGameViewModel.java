/*
 Copyright 2019 Blocks Global Pty Ltd (http://www.blocksglobal.com/)

 This file is part of Draughts.

 Draughts is free software:
 you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.

 Draughts is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package com.blocksglobal.lumin.draughts.viewmodel;

import androidx.databinding.Bindable;
import androidx.databinding.Observable;
import androidx.databinding.PropertyChangeRegistry;
import androidx.lifecycle.ViewModel;

import com.blocksglobal.lumin.draughts.BR;

public class NewGameViewModel extends ViewModel implements Observable {

    public static final int SINGLE_PLAYER = 0;
    public static final int MULTI_PLAYER = 1;
    private int modeSelected;
    private PropertyChangeRegistry registry = new PropertyChangeRegistry();


    @Bindable
    public boolean isSinglePlayerSelected() {
        return modeSelected == SINGLE_PLAYER;
    }

    @Bindable
    public boolean isMultiPlayerSelected() {
        return modeSelected == MULTI_PLAYER;
    }

    public void setModeSelected(int modeSelected) {
        this.modeSelected = modeSelected;
        registry.notifyChange(this, BR.singlePlayerSelected);
        registry.notifyChange(this, BR.multiPlayerSelected);
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.remove(callback);
    }
}
