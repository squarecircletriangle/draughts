/*
 Copyright 2019 Blocks Global Pty Ltd (http://www.blocksglobal.com/)

 This file is part of Draughts.

 Draughts is free software:
 you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.

 Draughts is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package com.blocksglobal.lumin.draughts.ui;


import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.blocksglobal.lumin.draughts.R;
import com.blocksglobal.lumin.draughts.databinding.FragmentNewGameModalBinding;
import com.blocksglobal.lumin.draughts.game.GameType;
import com.blocksglobal.lumin.draughts.viewmodel.NewGameViewModel;


public class NewGameModal extends DialogFragment implements NewGameModalEventHandler {

    public static final String KEY_CANCELABLE = "key_cancelable";
    private NewGameViewModel newGameViewModel;

    public NewGameModal() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        boolean isCancelable = false;
        Bundle bundle = getArguments();
        if (bundle != null) {
            isCancelable = bundle.getBoolean(KEY_CANCELABLE, false);
        }
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setCanceledOnTouchOutside(isCancelable);
        Window window = dialog.getWindow();
        if (window != null) {
            window.requestFeature(Window.FEATURE_NO_TITLE);
        }
        return dialog;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentNewGameModalBinding fragmentNewGameModalBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_new_game_modal, container, false);
        newGameViewModel = ViewModelProviders.of(this).get(NewGameViewModel.class);
        fragmentNewGameModalBinding.setViewModel(newGameViewModel);
        fragmentNewGameModalBinding.setEventHandler(this);
        return fragmentNewGameModalBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setLayout(700, 430);
        }
    }

    @Override
    public boolean onModeSelected(View view, MotionEvent motionEvent, int mode) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            newGameViewModel.setModeSelected(mode);
            view.performClick();
        }
        return false;
    }

    @Override
    public boolean onStartGameClick(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            Activity activity = getActivity();
            if (activity instanceof NewGameModalCallbacks) {
                if (newGameViewModel.isSinglePlayerSelected()) {
                    ((NewGameModalCallbacks) activity).onStartGame(GameType.Bot);
                } else {
                    ((NewGameModalCallbacks) activity).onStartGame(GameType.Human);
                }
                dismiss();
            }
            view.performClick();
        }
        return false;
    }
}
