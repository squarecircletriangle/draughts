/*
 Copyright 2019 Blocks Global Pty Ltd (http://www.blocksglobal.com/)

 This file is part of Draughts.

 Draughts is free software:
 you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.

 Draughts is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package com.blocksglobal.lumin.draughts.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.Nullable;

import com.blocksglobal.lumin.draughts.model.LuminUser;

import org.json.JSONException;
import org.json.JSONObject;

public class LuminUtils {

    private static final String COUNTLY_JSON_EVENT_NAME = "eventname";
    private static final String ANALYTICS_INTENT_SESSION = "com.blocksglobal.lumin.analytics.intent.";
    private static final String ANALYTICS_INTENT_EVENT = ANALYTICS_INTENT_SESSION + "EVENT";
    private static final String ANALYTICS_INTENT_DATA = "data";
    private static final String ANALYTICS_EVENT_DURATION = "eventduration";
    private static final String ANALYTICS_EVENT_SEGMENT = "eventsegment";

    public static @Nullable
    LuminUser getLuminUser(@Nullable Context context) {
        if (context != null) {
            Uri PROFILE_CONTENT_URI = Uri.parse("content://com.blocksglobal.lumen.account.profile/json");
            String[] args = {"json"};
            ContentResolver contentResolver = context.getContentResolver();
            String[] projection = {"content"};
            Cursor cursor = contentResolver.query(PROFILE_CONTENT_URI, projection, "type=?", args, null);
            if (cursor != null && cursor.moveToNext()) {
                String json = cursor.getString(cursor.getColumnIndex("content"));
                cursor.close();
                try {
                    JSONObject jsonObject = new JSONObject(json).getJSONObject("account");
                    String uuid = jsonObject.getString("uuid");
                    String fullName = jsonObject.getString("full_name");
                    String email = jsonObject.getString("email");
                    return new LuminUser(email, uuid, fullName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static void sendLuminAnalyticsEvent(@Nullable Context context, String eventName, long duration) {
        if (context == null) {
            return;
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(COUNTLY_JSON_EVENT_NAME, eventName);
            jsonObject.put(ANALYTICS_EVENT_DURATION, Long.toString(duration));
            jsonObject.put(ANALYTICS_EVENT_SEGMENT, new JSONObject());
            Intent intent = new Intent(ANALYTICS_INTENT_EVENT);
            intent.putExtra(ANALYTICS_INTENT_DATA, jsonObject.toString());
            context.sendBroadcast(intent);
        } catch (JSONException e) {
        }
    }
}
