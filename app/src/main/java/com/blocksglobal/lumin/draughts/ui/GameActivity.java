/*
 This file is part of Draughts.

 Draughts is free software:
 you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.

 Draughts is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This file was last modified by Blocks Global Pty Ltd (http://www.blocksglobal.com/) on 14/02/2019
 */

package com.blocksglobal.lumin.draughts.ui;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;
import static com.blocksglobal.lumin.draughts.app.Draughts.ANALYTICS_APP_LAUNCH;
import static com.blocksglobal.lumin.draughts.ui.EndOfGameModal.KEY_RESULT_TEXT;
import static com.blocksglobal.lumin.draughts.utils.LuminUtils.sendLuminAnalyticsEvent;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.blocksglobal.lumin.draughts.R;
import com.blocksglobal.lumin.draughts.game.Board;
import com.blocksglobal.lumin.draughts.game.CheckersGame;
import com.blocksglobal.lumin.draughts.game.GameType;
import com.blocksglobal.lumin.draughts.game.Move;
import com.blocksglobal.lumin.draughts.game.Piece;
import com.blocksglobal.lumin.draughts.game.Position;
import com.bumptech.glide.Glide;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class GameActivity extends AppCompatActivity implements NewGameModalCallbacks, EndOfGameModalCallbacks, PopupMenu.OnMenuItemClickListener {

    private Handler mHandler;
    private CheckersGame game;
    private CheckersLayout checkersView;
    private TextView currentPlayerText;
    private LinearLayout capturedBlackPiecesUI;
    private LinearLayout capturedWhitePiecesUI;
    boolean actionInProgress;
    private EndOfGameModal endOfGameModal;
    private NewGameModal newGameModal;
    Piece selectedPiece;
    Position selectedPosition;
    Piece selectablePieces[];
    Position moveOptions[];
    public static final String TEXT_DISPLAY = "TEXT_DISPLAY";
    public static final String INTENT_STATUS_BAR_MESSAGE = "com.blocksglobal.lumen.statusbar.message";
    public static final String INTENT_STATUS_BAR_RESET = "com.blocksglobal.lumen.statusbar.message.reset";
    private long startTime;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle saved) {
        super.onCreate(saved);
        mHandler = new Handler();
        game = loadFile();
        actionInProgress = false;
        setContentView(R.layout.activity_game);
        if (game == null) {
            new NewGameModal().show(getSupportFragmentManager(), null);
        } else {
            initCheckersBoard();
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(preferencesChangeListener);
        TextView textView = findViewById(R.id.newGameButton);
        textView.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                onNewGameClick();
                v.performClick();
            }
            return false;
        });
        Button button = findViewById(R.id.overflowMenu);
        button.setOnClickListener(v -> {
            PopupMenu popMenu = new PopupMenu(GameActivity.this, v, Gravity.END);
            popMenu.setOnMenuItemClickListener(GameActivity.this);
            popMenu.inflate(R.menu.overflow_menu);
            popMenu.show();
        });
    }

    private void initCheckersBoard() {
        checkersView = new CheckersLayout(game, this);
        checkersView.refresh();

        // layouts which contain all items displayed ingame
        LinearLayout mainContentLayout = findViewById(R.id.main_content);
        LinearLayout sideContentLayout = findViewById(R.id.side_content);
        mainContentLayout.removeAllViews();
        sideContentLayout.removeAllViews();

        mainContentLayout.addView(checkersView);

        // text which displays whose turn it is
        currentPlayerText = new TextView(this);
        currentPlayerText.setTextSize(30);
        currentPlayerText.setTextColor(getResources().getColor(R.color.textColor));
        currentPlayerText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        sideContentLayout.addView(currentPlayerText);

        // layouts for captured pieces
        capturedBlackPiecesUI = new LinearLayout(this);
        capturedBlackPiecesUI.setOrientation(LinearLayout.HORIZONTAL);

        capturedWhitePiecesUI = new LinearLayout(this);
        capturedWhitePiecesUI.setOrientation(LinearLayout.HORIZONTAL);

        sideContentLayout.addView(capturedBlackPiecesUI);
        sideContentLayout.addView(capturedWhitePiecesUI);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mHandler.removeCallbacksAndMessages(null);
        game = null;

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(preferencesChangeListener);
    }

    private ImageView generatePieceImage(int id) {
        ImageView image = new ImageView(this);


        int pixels = getResources().getConfiguration().orientation != ORIENTATION_LANDSCAPE ?
                Resources.getSystem().getDisplayMetrics().widthPixels / 12 :
                Resources.getSystem().getDisplayMetrics().widthPixels / 12 / 2;

        image.setLayoutParams(new LinearLayout.LayoutParams(pixels, pixels));
        switch (id) {
            case 1:
                Glide.with(this).load(game.getBlackNormalIconId()).into(image);
                break;
            case 2:
                Glide.with(this).load(game.getWhiteNormalIconId()).into(image);
                break;
            case 3:
                Glide.with(this).load(game.getBlackKingIconId()).into(image);
                break;
            default:
                Glide.with(this).load(game.getWhiteKingIconId()).into(image);
                break;
        }

        return image;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setLuminStatusBarWithAppName();
        if (game != null) {
            prepTurn();
        }
        startTime = System.currentTimeMillis();
    }

    // For Lumin screen users
    private void setLuminStatusBarWithAppName() {
        Intent intent = new Intent(INTENT_STATUS_BAR_MESSAGE);
        intent.putExtra(TEXT_DISPLAY, getString(R.string.app_name));
        sendBroadcast(intent);
    }

    //For Lumin screen users
    private void resetLuminStatusBar() {
        sendBroadcast(new Intent(INTENT_STATUS_BAR_RESET));
    }

    // prepare a human or computer turn
    public void prepTurn() {
        Board board = game.getBoard();

        selectedPiece = null;
        selectedPosition = null;
        selectablePieces = null;
        moveOptions = null;

        int turn = game.whoseTurn();

        if (game.getGameType() == GameType.Bot && turn == CheckersGame.BLACK) {
            currentPlayerText.setText(R.string.game_current_player_ai);

            mHandler.postDelayed(() -> {
                makeComputerTurn();
                actionInProgress = false;
            }, 1000);

        } else {
            if (turn == CheckersGame.BLACK)
                currentPlayerText.setText(R.string.game_current_player_black);
            else
                currentPlayerText.setText(R.string.game_current_player_white);
            // prep for human player turn
            ArrayList<Piece> selectablePieces = new ArrayList<>();
            Move moves[] = game.getMoves();

            // find pieces which can be moved
            for (Move move : moves) {
                Piece newPiece = board.getPiece(move.start());
                if (!selectablePieces.contains(newPiece)) {
                    selectablePieces.add(newPiece);
                }
            }

            // convert to array
            this.selectablePieces = selectablePieces.toArray(new Piece[0]);
            if (selectablePieces.size() == 0) {
                game.setGameFinished(true);
                // delete file
                deleteFile("savedata");
                showEndOfGameModal(turn);
            }
        }

        updateCapturedPiecesUI();
        checkersView.refresh();
    }

    // difficulty easy: randomly pick a move
    private void makeComputerTurn() {
        if (game.whoseTurn() == CheckersGame.BLACK) {
            Move moves[] = game.getMoves();
            if (moves.length > 0) {
                int num = (int) (moves.length * Math.random());
                final Move choice = moves[num];

                checkersView.animateMove(choice);

                mHandler.postDelayed(() -> {
                    if (game != null) {
                        game.makeMove(choice);
                        prepTurn();
                    }
                }, 1500);


            } else {
                // player wins
                game.setGameFinished(true);
                deleteFile("savedata");
                showEndOfGameModal(CheckersGame.BLACK);
            }
        }
    }

    private void updateCapturedPiecesUI() {
        int index;
        while (game.getCapturedBlackPieces().size() > capturedBlackPiecesUI.getChildCount()) {
            index = capturedBlackPiecesUI.getChildCount();
            capturedBlackPiecesUI.addView(generatePieceImage(game.getCapturedBlackPieces().get(index).getSummaryID()));
        }
        while (game.getCapturedWhitePieces().size() > capturedWhitePiecesUI.getChildCount()) {
            index = capturedWhitePiecesUI.getChildCount();
            capturedWhitePiecesUI.addView(generatePieceImage(game.getCapturedWhitePieces().get(index).getSummaryID()));
        }


    }

    // check which piece is selected
    public boolean isSelected(Piece piece) {
        return (piece != null && piece == selectedPiece);
    }

    // check which squares are options
    public boolean isOption(Position checkPosition) {
        if (moveOptions == null) {
            return false;
        }
        for (Position position : moveOptions) {
            if (position.equals(checkPosition)) {
                return true;
            }
        }
        return false;
    }

    public void selectPiece(Piece piece, Position location) {
        selectedPiece = null;
        selectedPosition = null;
        moveOptions = null;

        if (piece != null && selectablePieces != null
                && piece.getColor() == game.whoseTurn()) {
            boolean isSelectable = false;
            for (Piece selectablePiece : selectablePieces) {
                if (selectablePiece == piece) {
                    isSelectable = true;
                }
            }

            if (isSelectable) {
                selectedPiece = piece;
                selectedPosition = location;

                // fill move options

                ArrayList<Position> moveOptionsArr = new ArrayList<>();
                Move allMoves[] = game.getMoves();

                // iterate through moves
                for (Move checkMove : allMoves) {
                    Position start = checkMove.start();
                    Position end = checkMove.end();

                    if (start.equals(location)) {
                        if (!moveOptionsArr.contains(end)) {
                            moveOptionsArr.add(end);
                        }
                    }
                }
                // save list results
                moveOptions = moveOptionsArr.toArray(new Position[0]);
            }
        }

        checkersView.refresh();
    }

    // player made a move
    public void makeMove(Position destination) {
        // make longest move available
        final Move move = game.getLongestMove(selectedPosition, destination);
        if (move != null) {
            checkersView.animateMove(move);
            mHandler.postDelayed(() -> {
                if (game != null) {
                    game.makeMove(move);
                    prepTurn();
                    actionInProgress = false;
                }
            }, 1500);
        } else {
            actionInProgress = false;
        }
    }

    // player makes a click
    public void onClick(int x, int y) {
        if (!actionInProgress) {
            if (game.getGameType() != GameType.Bot || game.whoseTurn() != CheckersGame.BLACK) {
                Position location = new Position(x, y);
                Piece targetPiece = game.getBoard().getPiece(x, y);

                // attempting to make a move
                if (selectedPiece != null && selectedPosition != null && targetPiece == null) {
                    //game.advanceTurn();
                    actionInProgress = true;
                    makeMove(location);
                } else {
                    selectPiece(targetPiece, location);
                    if (selectedPiece == null)
                        checkersView.highlightSelectablePieces(selectablePieces);
                    mHandler.postDelayed(() -> checkersView.refresh(), 500);
                }
            }
        }
    }


    private SharedPreferences.OnSharedPreferenceChangeListener preferencesChangeListener =
            (sharedPreferences, s) -> {
                // update preferences
                prepTurn();
            };

    /**
     * Shows the dialog after a game was won with the options of going back to main as well as showing the final game pane
     */
    public void showEndOfGameModal(int turn) {
        endOfGameModal = new EndOfGameModal();
        Bundle bundle = new Bundle();
        if (turn == CheckersGame.BLACK) {
            bundle.putString(KEY_RESULT_TEXT, getString(R.string.white_victorious));
        } else {
            bundle.putString(KEY_RESULT_TEXT, getString(R.string.black_victorious));
        }
        endOfGameModal.setArguments(bundle);
        endOfGameModal.show(getSupportFragmentManager(), null);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state

        savedInstanceState.putParcelable("game", game);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        //super.onRestoreInstanceState(savedInstanceState);
    }

    public void onPause() {
        super.onPause();
        resetLuminStatusBar();
        sendLuminAnalyticsEvent(getApplicationContext(), ANALYTICS_APP_LAUNCH, (System.currentTimeMillis() - startTime) / 1000);
        if (game != null && !game.isGameFinished()) {
            //state will be saved in a file
            FileOutputStream fos = null;
            ObjectOutputStream oos = null;
            try {
                fos = openFileOutput("savedata", Context.MODE_PRIVATE);
                oos = new ObjectOutputStream(fos);
                oos.writeObject(game);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (oos != null) try {
                    oos.close();
                } catch (IOException ignored) {
                }
                if (fos != null) try {
                    fos.close();
                } catch (IOException ignored) {
                }
            }
        }
        if (endOfGameModal != null) {
            endOfGameModal.dismiss();
            endOfGameModal = null;
        }
        if (newGameModal != null) {
            newGameModal.dismiss();
            newGameModal = null;
        }
    }

    private CheckersGame loadFile() {
        ObjectInputStream ois = null;
        FileInputStream fis = null;
        try {
            fis = this.openFileInput("savedata");
            ois = new ObjectInputStream(fis);
            game = (CheckersGame) ois.readObject();
            return game;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (ois != null) try {
                ois.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (fis != null) try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public void onStartGame(GameType gameType) {
        game = new CheckersGame(gameType);
        initCheckersBoard();
        prepTurn();
    }

    @Override
    public void onNewGameClick() {
        newGameModal = new NewGameModal();
        Bundle extras = new Bundle();
        extras.putBoolean(NewGameModal.KEY_CANCELABLE, true);
        newGameModal.setArguments(extras);
        newGameModal.show(getSupportFragmentManager(), null);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }
}


