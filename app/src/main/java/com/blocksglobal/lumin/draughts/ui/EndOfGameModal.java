/*
 Copyright 2019 Blocks Global Pty Ltd (http://www.blocksglobal.com/)

 This file is part of Draughts.

 Draughts is free software:
 you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.

 Draughts is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package com.blocksglobal.lumin.draughts.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.blocksglobal.lumin.draughts.R;

public class EndOfGameModal extends DialogFragment {

    public static final String KEY_RESULT_TEXT = "key_result_text";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setCanceledOnTouchOutside(false);
        Window window = dialog.getWindow();
        if (window != null) {
            window.requestFeature(Window.FEATURE_NO_TITLE);
        }
        return dialog;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_end_of_game, container);
        TextView gameResultText = view.findViewById(R.id.gameResultText);
        Bundle bundle = getArguments();
        if (bundle != null) {
            gameResultText.setText(bundle.getString(KEY_RESULT_TEXT));
        }
        TextView exitButton = view.findViewById(R.id.exitButton);
        exitButton.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                if (getActivity() != null) {
                    dismiss();
                    getActivity().finish();
                }
                v.performClick();
            }
            return false;
        });
        TextView newGameButton = view.findViewById(R.id.newGameButton);
        newGameButton.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Activity activity = getActivity();
                if (activity instanceof EndOfGameModalCallbacks) {
                    dismiss();
                    ((EndOfGameModalCallbacks) activity).onNewGameClick();
                }
                v.performClick();
            }
            return false;
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        adjustWindowSize();
    }

    private void adjustWindowSize() {
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setLayout(700, 300);
        }
    }
}
